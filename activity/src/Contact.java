import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> contactNumber;
    private ArrayList<String> address;


    public Contact(){}
    public Contact(String name, ArrayList<String> contactNumber, ArrayList<String> address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<String> getContactNumber() {
        return this.contactNumber;
    }

    public ArrayList<String> getAddress() {
        return this.address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber(ArrayList<String> contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAddress(ArrayList<String> address) {
        this.address = address;
    }
}
