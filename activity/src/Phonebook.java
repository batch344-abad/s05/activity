import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts;


    public Phonebook(){
        this.contacts = new ArrayList<>();
    }


    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

}
