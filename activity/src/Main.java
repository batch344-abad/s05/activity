import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("John Doe");

        ArrayList<String> contactNumbers1 = new ArrayList<>();
        contactNumbers1.add("+639152468596");
        contactNumbers1.add("+639228547963");
        contact1.setContactNumber(contactNumbers1);

        ArrayList<String> addresses1 = new ArrayList<>();
        addresses1.add("my home in Quezon City");
        addresses1.add("my office in Makati City");
        contact1.setAddress(addresses1);

        ArrayList<String> contactNumbers2 = new ArrayList<>();
        contactNumbers2.add("+639162148573");
        contactNumbers2.add("+639173698541");

        ArrayList<String> addresses2 = new ArrayList<>();
        addresses2.add("my home in Caloocan City");
        addresses2.add("my office in Pasay");

        Contact contact2 = new Contact("Jane Doe", contactNumbers2, addresses2);

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String contactNumber : contact.getContactNumber()) {
                    System.out.println(contactNumber);
                }

                System.out.println("-----------------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                for (String address : contact.getAddress()) {
                    System.out.println(address);
                }
                System.out.println("===================================");
            }
        }
    }
}
